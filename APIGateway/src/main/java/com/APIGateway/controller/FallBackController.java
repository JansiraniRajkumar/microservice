package com.APIGateway.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.APIGateway.config.SplunkConfig;
import com.splunk.Receiver;
import com.splunk.Service;

import reactor.core.publisher.Mono;

@RestController
public class FallBackController {
	
	Service service = SplunkConfig.getSplunkService();
	Receiver receiver = service.getReceiver();
	
	@RequestMapping("/departmentFallBack")
	public Mono<String> getdepartmentFallback(){
		receiver.log("dept", "From Api Gateway application Fall Back Method");
		return Mono.just("Service is down.Please try after some times");
		
		
	}

}
