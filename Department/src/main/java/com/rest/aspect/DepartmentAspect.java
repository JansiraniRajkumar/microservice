package com.rest.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.rest.model.Department;


@Aspect
@Component
public class DepartmentAspect {
	
	Logger logger = LoggerFactory.getLogger(DepartmentAspect.class);  
	
	@Before(value = "execution(* com.rest.service.DepartmentService.insertDepartment(..)) and args(dept)")
	public void beforeAdvice(JoinPoint joinPoint, Department dept) {
		System.out.println("Before method:" + joinPoint.getSignature());
		System.out.println("Creating Department with department Id - " + dept.getDeptId() + ", department name - "
				+ dept.getDeptName());
		logger.info("Before Advice Method in Department application");
				

	}

	@After(value = "execution(* com.rest.service.DepartmentService.insertDepartment(..)) and args(dept)")
	public void afterAdviceMethod(JoinPoint joinPoint,Department dept) {
		System.out.println("After method:" + joinPoint.getSignature());
		System.out.println("Creating Department with department Id - " + dept.getDeptId() + ", department name - "
				+ dept.getDeptName());
		logger.info("After Advice Method in Department application");		
	}

	@AfterReturning(value = "execution(* com.rest.service.DepartmentService.insertDepartment(..)) and args(dept)", returning = "result")
	public void afterReturning(JoinPoint joinPoint,Department dept, Object result) {
		System.out.println("Method : " + joinPoint.getSignature() + " result : " + result);
		logger.info("After returning advice Method in Department application");
	}


	@Around(value = "execution(* com.rest.service.DepartmentService.insertDepartment(..)) and args(dept)")
	public void aroundAdvice(ProceedingJoinPoint joinPoint, Department dept ) throws Throwable {
		System.out.println("Around method: " + joinPoint.getSignature());
		joinPoint.proceed();
		logger.info("Around Advice Method in Department application");
		

	}   

	@Pointcut(value = "execution(* com.rest.service.DepartmentService.deleteDepartment(..))")
	private void getEmployeesById() {
	}

	@AfterThrowing(value = "getEmployeesById()", throwing = "exception")
	public void afterThrowingAdvice(JoinPoint jp, Throwable exception) {
		System.out.println("Inside afterThrowingAdvice() method : " + jp.getSignature().getName() + " method");
		System.out.println("Exception= " + exception);
		logger.info("After Throwing Advice Method in Department application");
	}   

	
	
	

}
