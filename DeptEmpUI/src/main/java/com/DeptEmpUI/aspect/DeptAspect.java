package com.DeptEmpUI.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



@Aspect
@Component
public class DeptAspect {
	
	Logger logger = LoggerFactory.getLogger(DeptAspect.class);  

	@Before(value = "execution(* com.DeptEmpUI.controller.DeptController.deleteDepartment(..))")
	public void beforeAdvice(JoinPoint joinPoint) {
		System.out.println(" From DeptEmpUI Application Before method:" + joinPoint.getSignature());
		logger.info("Before Advice Method in DeptEmpUI application delete department");
		
	}

	@After(value = "execution(* com.DeptEmpUI.controller.DeptController.deleteDepartment(..))")
	public void afterAdviceMethod(JoinPoint joinPoint) {
		System.out.println("From DeptEmpUI Application After method: " + joinPoint.getSignature());
		logger.info("After Advice Method in DeptEmpUI application delete department");

	}

	
	@Before(value = "execution(* com.DeptEmpUI.controller.EmployeeController.deleteEmployee(..))")
	public void beforeAdviceEmployee(JoinPoint joinPoint) {
		System.out.println("From DeptEmpUI Application Before method:" + joinPoint.getSignature());
		logger.info("Before Advice Method in DeptEmpUI application delete employee");
		
	}

	@After(value = "execution(* com.DeptEmpUI.controller.DeptController.deleteEmployee(..))")
	public void afterAdviceEmployee(JoinPoint joinPoint) {
		System.out.println(" From DeptEmpUI Application After method:" + joinPoint.getSignature());
		logger.info("Before Advice Method in DeptEmpUI application delete employee");
		
	}   


}
